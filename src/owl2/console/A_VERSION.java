/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2.console;

/**
 * CONSOLE
 */
public class A_VERSION {

  final static public String app = "owl2_console";
  final static public int version = 1;
  final static public int owl2_lib_min = 1;
  final static public int owl2_service_lib_min = 1;
  //
  final static public int owl2_service_min = 1; // usado no login ao servico

  /*
   * 25/10/2020 1
   * inicial
   */
  static public void check(){
    System.out.println(app+".version="+version);
    owl2_service_lib.A_VERSION.check(app, owl2_service_lib_min);
    owl2_lib.A_VERSION.check(app, owl2_lib_min);
  }

}
