/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2.console.tcp;

import java.io.IOException;
import owl2.console.FConsole;
import owl2.console.sensor.FSensorList;
import owl2_lib.IPeer;
import owl2_lib.cmd.ICMD_Handler;
import owl2_lib.cmd.TCMD_TX;
import owl2_lib.cmd.TCMD_ack;
import owl2_lib.cmd.TCMD_c;
import owl2_lib.cmd.TCMD_j;
import owl2_lib.cmd.TCMD_l;
import owl2_lib.cmd.TCMD_r_a;
import owl2_lib.cmd.TCMD_r_p;
import owl2_lib.cmd.TCMD_r_s;
import owl2_lib.cmd.TCMD_u_k;
import owl2_lib.sensor.ESensorType;
import owl2_lib.sensor.ILog;
import owl2_lib.sensor.TPack;
import owl2_lib.sensor.TSensor;
import x.etc.io.socket.tcp.TXTCP;
import x.etc.io.socket.tcp.TXTCPClient;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;
import x.etc.lib;
import x.etc.timer.TXTimer;

/**
 *
 */
public class TServiceClient extends TXTCPClient implements ICMD_Handler, ILog {

  final static private int HEADER_LEN = 10;
  //
  final public FConsole fconsole;
  final public FSensorList fsensorlist;
  //
  public int sn;
  public String user_name;
  //
  final private byte[] header;
  private int headerCount;
  private int bufferCount;
  final private TPack pack;
  //
  final private TXTimer timer_ping;

  public TServiceClient(final TXTCP tcp_, final FConsole c, final FSensorList l){
    super(tcp_);
    fconsole = c;
    fsensorlist = l;
    header = new byte[HEADER_LEN];
    pack = new TPack(false);
    timer_ping = new TXTimer(30000) {
      @Override
      public void run(){
        send_ping();
      }
    };
    timer_ping.start();
  }

  @Override
  protected void onConnect(){
    fconsole.setVisible(false);
    fsensorlist.setVisible(true);
    send_login(user_name);
  }

  @Override
  public void onCantConnect(){
    fconsole.set_edits_enabled(true);
    fconsole.set_message("não foi possível conectar");
  }

  @Override
  public void onDisconnect(){
    fconsole.set_message("desconectado do servidor");
    fconsole.setVisible(true);
    fconsole.set_edits_enabled(true);
    fsensorlist.setVisible(false);
  }

  @Override
  protected void onDataAvailable(final byte[] data){

    int index = 0;
    try{
      while(index<data.length){
        if(headerCount<HEADER_LEN){
          final int needed = HEADER_LEN-headerCount;
          final int available = data.length-index;
          final int until = available<needed ? available : needed;
          for(int i = 0; i<until; i++){
            header[headerCount++] = data[index+i];
          }
          index += until;
          //
          if(header[0]!='u'){
            lib.unhandledException("peer", new Exception("header inválido"));
            index = data.length;
            close();
          }else if(headerCount==HEADER_LEN){
            pack.read_header(header);
            int check = lib.checkSum(header, 0, HEADER_LEN-4-1)&0xFFFF;
            if(check==pack.checksum_h){
              bufferCount = 0;
            }else{
              lib.unhandledException("peer", new Exception("checksum_h error"));
              index = data.length;
              close();
            }
          }
        }
        if(headerCount==HEADER_LEN){
          final int needed = pack.len-bufferCount;
          final int available = data.length-index;
          final int until = available<needed ? available : needed;
          for(int i = 0; i<until; i++){
            pack.buffer[bufferCount++] = data[index+i];
          }
          index += until;
          //
          if(bufferCount==pack.len){
            // completou os dados
            if((lib.checkSum(pack.buffer, 0, pack.len-1)&0xFFFF)==pack.checksum_d){
              pack.read_body_and_execute(this);
              // proximo pacote
              headerCount = 0;
            }else{
              lib.unhandledException("gprs peer", new Exception("checksum_d error"));
              index = data.length;
              close();
            }
          }
        }

      }
    }catch(final Exception ex){
      lib.unhandledException(ex);
      close();
    }
  }

  @Override
  public void execute_pack_c(final TPack p, final TCMD_c c){
    try{
      final TXStreamReader s = TXStreamReader.create(c.cmd);
      int v = s.readInt();
      switch(s.readShortString()){
        case "message":
          execute_message(s);
          break;

        case "sensor":
          execute_sensor(s);
          break;
      }
    }catch(final IOException ex){
    }
  }

  private void execute_message(final TXStreamReader s) throws IOException{
    fsensorlist.print("msg: "+s.readString());
  }

  private void execute_sensor(final TXStreamReader s) throws IOException{
    final int sn = s.readInt();
    final TSensor sensor = new TSensor(sn, ESensorType.NONE, null, this);
    fsensorlist.print(sensor.get_info());
  }

  @Override
  public void send_ping(){
    final TCMD_TX tx = new TCMD_TX();
    try{
      send(tx.u_ask_ping(sn));
    }catch(final Exception ex){
      close();
      ex.printStackTrace();
    }
  }

  public void send_login(final String name){
    user_name = name;
    final TCMD_TX tx = new TCMD_TX();
    try{
      send(tx.u_ask_j(0, sn, name));
    }catch(final Exception ex){
      close();
      ex.printStackTrace();
    }
  }

  public void send_finish(){
    final TCMD_TX tx = new TCMD_TX();
    try{
      final TXStreamWriter s = TXStreamWriter.create();
      s.writeShortString("finish");
      send(tx.u_ask_c(0, sn, s.getByteArray()));
    }catch(final Exception ex){
      close();
      ex.printStackTrace();
    }
  }

  public void send_sensor_watch(final byte[] ba){
    final TCMD_TX tx = new TCMD_TX();
    try{
      final TXStreamWriter s = TXStreamWriter.create();
      s.writeShortString("sensor_watch");
      s.writeByteArrayOnly(ba);
      send(tx.u_ask_c(0, sn, s.getByteArray()));
    }catch(final Exception ex){
      close();
      ex.printStackTrace();
    }
  }

  @Override
  public void print(final String msg){
    System.out.println("print: "+msg);
  }

  @Override
  public void print(final TSensor sensor, final String msg){
    System.out.println("sensor "+sensor.sn+": "+msg);
  }

  @Override
  public int get_sn(){
    return sn;
  }

  @Override
  public boolean is_gprs_main(int sn){
    return false;
  }

  @Override
  public boolean is_remote_control(){
    return false;
  }

  @Override
  public void peer_connected(final IPeer peer_){
  }

  @Override
  public void peer_disconnected(final IPeer peer_){
  }

  @Override
  public void pack_invalid(final TPack p){
  }

  @Override
  public void send_ack(final int n){
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void execute_pack_peer_done(){
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void execute_pack_j(final TPack p, final TCMD_j c){
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void execute_pack_l(final TPack p, final TCMD_l c){
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void execute_pack_ack(final TPack p, final TCMD_ack c){
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void execute_pack_r_a(final TPack p, final TCMD_r_a c){
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void execute_pack_r_p(final TPack p, final TCMD_r_p c){
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void execute_pack_r_s(final TPack p, final TCMD_r_s c){
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void execute_pack_u_k(final TPack p, final TCMD_u_k c){
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void on_exception(final String msg, final Exception ex){
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

}
